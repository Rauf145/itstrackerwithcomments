import Services.Devices;
import Services.Service;
import io.netty.channel.Channel;

import java.net.SocketAddress;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WialonProtocolDecoder {
    private static final Pattern PATTERN = new PatternBuilder()
            .number("(dd)(dd)(dd);")             // date (ddmmyy)
            .number("(dd)(dd)(dd);")             // time (hhmmss)
            .number("(dd)(dd.d+);")              // latitude
            .expression("([NS]);")
            .number("(ddd)(dd.d+);")             // longitude
            .expression("([EW]);")
            .number("(d+.?d*)?;")                // speed
            .number("(d+.?d*)?;")                // course
            .number("(?:NA|(-?d+.?d*));")        // altitude
            .number("(?:NA|(d+))")               // satellites
            .groupBegin().text(";")
            .number("(?:NA|(d+.?d*));")          // hdop
            .number("(?:NA|(d+));")              // inputs
            .number("(?:NA|(d+));")              // outputs
            .expression("(?:NA|([^;]*));")       // adc
            .expression("(?:NA|([^;]*));")       // ibutton
            .expression("(?:NA|(.*))")           // params
            .groupEnd("?")
            .compile();

//    private void sendResponse(Channel channel, SocketAddress remoteAddress, String type, Integer number) {
//        if (channel != null) {
//            StringBuilder response = new StringBuilder("#A");
//            response.append(type.substring(1));
//            if (number != null) {
//                response.append(number);
//            }
//            response.append("\r\n");
//            channel.writeAndFlush(new NetworkMessage(response.toString(), remoteAddress));
//        }
//    }

    private Position decodePosition(String substring) {
        Parser parser = new Parser(PATTERN, substring);
        if (!parser.matches()) {
            System.out.println("Error when i try parse position");
            return null;
        }
        //getProtocolName()
        Position position = new Position("Wialon");
        position.setDeviceId(12345);

        position.setTime(parser.nextDateTime(Parser.DateTimeFormat.DMY_HMS));

        position.setLatitude(parser.nextCoordinate());
        position.setLongitude(parser.nextCoordinate());
        position.setSpeed(UnitsConverter.knotsFromKph(parser.nextDouble(0)));
        position.setCourse(parser.nextDouble(0));
        position.setAltitude(parser.nextDouble(0));

        if (parser.hasNext()) {
            int satellites = parser.nextInt(0);
            position.setValid(satellites >= 3);
            position.set(Position.KEY_SATELLITES, satellites);
        }

        position.set(Position.KEY_HDOP, parser.nextDouble());
        position.set(Position.KEY_INPUT, parser.next());
        position.set(Position.KEY_OUTPUT, parser.next());

        if (parser.hasNext()) {
            String[] values = parser.next().split(",");
            for (int i = 0; i < values.length; i++) {
                position.set(Position.PREFIX_ADC + (i + 1), values[i]);
            }
        }

        position.set(Position.KEY_DRIVER_UNIQUE_ID, parser.next());

        if (parser.hasNext()) {
            String[] values = parser.next().split(",");
            for (String param : values) {
                Matcher paramParser = Pattern.compile("(.*):[1-3]:(.*)").matcher(param);
                if (paramParser.matches()) {
                    try {
                        position.set(paramParser.group(1).toLowerCase(), Double.parseDouble(paramParser.group(2)));
                    } catch (NumberFormatException e) {
                        position.set(paramParser.group(1).toLowerCase(), paramParser.group(2));
                    }
                }
            }
        }

        return position;
    }

    protected Object decode(Object msg) throws Exception {

        String sentence = (String) msg;
        String type = sentence.substring(0, sentence.indexOf('#', 1) + 1);

        switch (type) {
            case "#L#":
                String[] values = sentence.substring(3).split(";");

                String imei = values[0].indexOf('.') >= 0 ? values[1] : values[0];
//                System.out.println(imei);
                return Service.Get().find(imei);

//                DeviceSession deviceSession = getDeviceSession(channel, remoteAddress, imei);
//                if (deviceSession != null) {
//                    sendResponse(channel, remoteAddress, type, 1);
//                }
            case "#P#":
//                sendResponse(channel, remoteAddress, type, null); // heartbeat
                break;

            case "#D#":
            case "#SD#":
                Position position = decodePosition(sentence.substring(sentence.indexOf('#', 1) + 1));
                if (position != null) {
//                    sendResponse(channel, remoteAddress, type, 1);
                    return position;
                }
                break;

            case "#B#":
                String[] messages = sentence.substring(sentence.indexOf('#', 1) + 1).split("\\|");
                List<Position> positions = new LinkedList<>();

                for (String message : messages) {
                    position = decodePosition(message);
//                    if (position != null) {
//                        position.set(Position.KEY_ARCHIVE, true);
//                        positions.add(position);
//                    }
                }

//                sendResponse(channel, remoteAddress, type, messages.length);
                if (!positions.isEmpty()) {
                    return positions;
                }
                break;

//            case "#M#":
////                deviceSession = getDeviceSession(channel, remoteAddress);
////                if (deviceSession != null) {
//                    position = new Position("Wialon");
//                    position.setDeviceId(deviceSession.getDeviceId());
//                    getLastLocation(position, new Date());
//                    position.setValid(false);
//                    position.set(Position.KEY_RESULT, sentence.substring(sentence.indexOf('#', 1) + 1));
//                    sendResponse(channel, remoteAddress, type, 1);
//                    return position;
////                }
//                break;

            default:
                break;

        }

        return null;
    }

}
