package Protocols;


import Models.Message;
import org.codehaus.jackson.map.ObjectMapper;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WialonProtocolDecoder {

    private java.sql.Connection Connection = null;
    public static double kf = 0.00001;//test
    public static int[] imeis = new int[]{12345, 54321};//test

    List<String> users = null;
    private ObjectMapper gson = null;


    public WialonProtocolDecoder() {
//        String url = "jdbc:postgresql://127.0.0.1:5432/test";
//        String user = "postgres";
//        String password = "cls";
        users = new ArrayList<>();
        try {
//            Class.forName("org.postgresql.Driver");
//            Connection = DriverManager.getConnection(url, user, password);
//
//            PreparedStatement pst = Connection.prepareStatement("SELECT uniqueid FROM tc_devices");
//            ResultSet rs = pst.executeQuery();
//
//            while (rs.next()) {
//                users.add(rs.getString(1));
//            }

            this.gson = new ObjectMapper();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private Pattern PATTERN = new PatternBuilder()
            .number("(dd)(dd)(dd);")             // date (ddmmyy)
            .number("(dd)(dd)(dd);")             // time (hhmmss)
            .number("(dd)(dd.d+);")              // latitude
            .expression("([NS]);")
            .number("(ddd)(dd.d+);")             // longitude
            .expression("([EW]);")
            .number("(d+.?d*)?;")                // speed
            .number("(d+.?d*)?;")                // course
            .number("(?:NA|(-?d+.?d*));")        // altitude
            .number("(?:NA|(d+))")               // satellites
            .groupBegin().text(";")
            .number("(?:NA|(d+.?d*));")          // hdop
            .number("(?:NA|(d+));")              // inputs
            .number("(?:NA|(d+));")              // outputs
            .expression("(?:NA|([^;]*));")       // adc
            .expression("(?:NA|([^;]*));")       // ibutton
            .expression("(?:NA|(.*))")           // params
            .groupEnd("?")
            .compile();

//    private void sendResponse(Channel channel, SocketAddress remoteAddress, String type, Integer number) {
//        if (channel != null) {
//            StringBuilder response = new StringBuilder("#A");
//            response.append(type.substring(1));
//            if (number != null) {
//                response.append(number);
//            }
//            response.append("\r\n");
//            channel.writeAndFlush(new NetworkMessage(response.toString(), remoteAddress));
//        }
//    }

    private Position decodePosition(String substring) {
//        System.out.println(substring);
        Parser parser = new Parser(PATTERN, substring);
        parser.matches();
        if (!parser.matches()) {
            System.out.println("Error when i try parse position");
            //return null;
        }
        //getProtocolName()
        Position position = new Position("Wialon");
//        position.setDeviceId(imeis[num]);//test
//        System.out.println(num);

        position.setTime(parser.nextDateTime(Parser.DateTimeFormat.DMY_HMS));

        position.setLatitude(parser.nextCoordinate());
        position.setLongitude(parser.nextCoordinate());
        position.setSpeed(UnitsConverter.knotsFromKph(parser.nextDouble(0)));
        position.setCourse(parser.nextDouble(0));
        position.setAltitude(parser.nextDouble(0));

        if (parser.hasNext()) {
            int satellites = parser.nextInt(0);
            position.setValid(satellites >= 3);
            position.set(Position.KEY_SATELLITES, satellites);
        }

        position.set(Position.KEY_HDOP, parser.nextDouble());
        position.set(Position.KEY_INPUT, parser.next());
        position.set(Position.KEY_OUTPUT, parser.next());

        if (parser.hasNext()) {
            String[] values = parser.next().split(",");
            for (int i = 0; i < values.length; i++) {
                position.set(Position.PREFIX_ADC + (i + 1), values[i]);
            }
        }

        position.set(Position.KEY_DRIVER_UNIQUE_ID, parser.next());

        if (parser.hasNext()) {
            String[] values = parser.next().split(",");
            for (String param : values) {
                Matcher paramParser = Pattern.compile("(.*):[1-3]:(.*)").matcher(param);
                if (paramParser.matches()) {
                    try {
                        position.set(paramParser.group(1).toLowerCase(), Double.parseDouble(paramParser.group(2)));
                    } catch (NumberFormatException e) {
                        position.set(paramParser.group(1).toLowerCase(), paramParser.group(2));
                    }
                }
            }
        }

        return position;
    }

    public Message Decode(Message msg) throws Exception {

        String sentence = new String(msg.Content, StandardCharsets.UTF_8);
        sentence = sentence.replace("\r\n", "").replace("\u0000", "");
        if (sentence.contains("#L"))
            sentence = sentence.split("#D")[0];
        String type = sentence.substring(0, sentence.indexOf('#', 1) + 1);
        System.out.println(sentence);
        switch (type) {
            case "#L#":
                String[] values = sentence.substring(3).split(";");

                String imei = values[0].indexOf('.') >= 0 ? values[1] : values[0];

                if (imei != null) {
//                    DeviceManager.GetManager().Add(msg.Client, imei);
                    String response = "#AL1\r\n";
                    msg.Type = "Login";
                    msg.LoggedIn = true;
                    msg.Imei = imei;
                    msg.Response = response.getBytes();
                    return msg;
                }
                return null;


            case "#P#":
//                sendResponse(channel, remoteAddress, type, null); // heartbeat
                break;

            case "#D#":
            case "#SD#":
                Position position = decodePosition(sentence.substring(sentence.indexOf('#', 1) + 1));
                System.out.println(position.toString());
                if (position != null) {
//                    int num = (int) (Math.random() * 2);//test
//                    position.setDeviceId(imeis[num]);//test
//                    double lon = 49.8671;
//                    if (num == 0)
//                        lon = 49.8672;

//                    System.out.println(num);
//                    String Imei = DeviceManager.GetManager().Get(msg.Client);
//                    System.out.println(Imei);
//                    if (Imei != null)
//                        position.setDeviceId(Long.parseLong(Imei));
//                    else {
//                        position.setDeviceId(12345);
//                        position.setLongitude((49.8672 +kf)%50);
//                        position.setLatitude((40.4093 +kf)%50);

//                        kf+= 0.00001;
//                    }
                    msg.Response = "#AD#1\\r\\n".getBytes();
                    msg.Type = "position";
                    msg.Content = gson.writeValueAsString(position).getBytes();
                    return msg;
                }
                break;

            case "#B#":
                String[] messages = sentence.substring(sentence.indexOf('#', 1) + 1).split("\\|");
                List<Position> positions = new LinkedList<>();

                for (String message : messages) {
//                    position = decodePosition(message);
//                    if (position != null) {
//                        position.set(Position.KEY_ARCHIVE, true);
//                        positions.add(position);
//                    }
                }

//                sendResponse(channel, remoteAddress, type, messages.length);
                if (!positions.isEmpty()) {
                    msg.Type = "position";
                    msg.Content = gson.writeValueAsString(positions).getBytes();
                    return msg;
                }
                break;

//            case "#M#":
////                deviceSession = getDeviceSession(channel, remoteAddress);
////                if (deviceSession != null) {
//                    position = new Position("Wialon");
//                    position.setDeviceId(deviceSession.getDeviceId());
//                    getLastLocation(position, new Date());
//                    position.setValid(false);
//                    position.set(Position.KEY_RESULT, sentence.substring(sentence.indexOf('#', 1) + 1));
//                    sendResponse(channel, remoteAddress, type, 1);
//                    return position;
////                }
//                break;

            default:
                break;

        }

        return null;
    }

}
