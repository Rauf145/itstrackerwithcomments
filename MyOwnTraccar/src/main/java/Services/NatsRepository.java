package Services;

import io.nats.client.Connection;
import io.nats.client.Dispatcher;
import io.nats.client.Message;
import io.nats.client.Nats;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.HashMap;

public class NatsRepository {

    private ObjectMapper objectMapper = null;
    private Connection nc = null;
    //Nats object
    private Dispatcher d = null;
    public HashMap<String, Channel> Channels = null;

    private static NatsRepository _instance = null;

    private NatsRepository() {
        try {
            this.objectMapper = new ObjectMapper();
            this.nc = Nats.connect("localhost:4222");
            this.d = this.nc.createDispatcher((msg) -> {
                this.Handle(msg);
            });
            //listen message queue 'repsonse' from DecodeWorker (For example device send data for login. After decoding DecodeWorker send response to backEnd
            //For example : #L#12312323 ...  . Response from DecoderWorker is a #AL#1 that means - good (Wialon)
            d.subscribe("Response", "workers");
            this.Channels = new HashMap<>();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static NatsRepository GetInstance() {
        if (_instance == null) {
            _instance = new NatsRepository();
        }
        return _instance;
    }
    //Add Device to list
    public void AddChannel(Channel channel) {
        this.Channels.put(channel.remoteAddress().toString(), channel);
    }
    //Remove Device from list
    public void RemoveChannel(Channel channel) {
        this.Channels.remove(channel.remoteAddress().toString());
    }

    //Send data to message queue
    public void Send(byte[] data) {
        this.nc.publish("messages", data);
    }


    //Recieve message from DecodeWorker
    private void Handle(Message msg) {
            try {
            //get data from message queue
            String str = new String(msg.getData(), "UTF-8");
            //convert from json to Message object(my class)
            Models.Message message = this.objectMapper.readValue(str, Models.Message.class);
            //Find device by address
            if (this.Channels.containsKey(message.Client)) {
                //send response to client
                this.Channels
                             .get(message.Client)
                             .writeAndFlush(Unpooled.wrappedBuffer(message.Response));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
