package Services;

import Models.Message;

import javax.swing.text.Position;

public interface IService {
    public boolean find(String imei);
    public boolean send(Message msg);
}
