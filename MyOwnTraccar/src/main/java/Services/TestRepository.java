package Services;

import com.sun.xml.internal.ws.dump.LoggingDumpTube;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestRepository{

    private Connection connection;

    private List<String> devices;

    private static Statement st;

    static FileWriter writer;

    private static int Count = 0;




    public TestRepository() {
        String url = "jdbc:postgresql://127.0.0.1:5432/traccar";
        String user = "pgdbuser";
        String password = "$R5t6y!Q";

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
            devices = new ArrayList<>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT uniqueId FROM tc_devices;");
            while (rs.next()){
                devices.add(rs.getString(1));
            }
            TestRepository.st = connection.createStatement();
        } catch(Exception ex) {
            System.out.println("Error when i try connect to database(pg)   TestRepository constructor " + ex.getMessage());
        }
    }


    public boolean FindImei(String imei){
        return devices.contains(imei);

    }


    public  boolean InsertPosition(String position){
        try{
            Statement st = connection.createStatement();
            st.execute(String.format("INSERT INTO tc_temp(" +
                    "protocol, deviceid, servertime, devicetime, fixtime, valid, latitude, longitude, altitude) " +
                    "VALUES (%s);", position));
            return true;

        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
}




//            if (rs.next()) {
//                System.out.println(rs.getString(1));
//            }