package Services;

import Models.Message;
import org.codehaus.jackson.map.ObjectMapper;

public class Service implements IService {

    private static Service _instance = null;

    private NatsRepository natsRepository = null;//*

    private ObjectMapper gson = null;

    private Service() {
        this.natsRepository = NatsRepository.GetInstance();
        this.gson = new ObjectMapper();
    }

    public static Service Get() {
        if (_instance == null) {
            _instance = new Service();
        }
        return _instance;
    }

    @Override
    public boolean find(String imei) {
        return true;
    }

    @Override
    public boolean send(Message msg) {
        try {
            //convert Message (my class) into json
            String json = this.gson.writeValueAsString(msg);
            //send to server
            this.natsRepository.Send(json.getBytes());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return true;
    }
}
