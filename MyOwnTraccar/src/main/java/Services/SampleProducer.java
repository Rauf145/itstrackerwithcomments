package Services;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;


public class SampleProducer {
    private final KafkaProducer producer;
    private String topic = "";
    private Boolean isAsync = false;
    public static final String KAFKA_SERVER_URL = "localhost";
    public static final int KAFKA_SERVER_PORT = 9092;
//    public static final String CLIENT_ID = "SampleProducer";

    private static SampleProducer _instance = null;

    private SampleProducer(String topic, Boolean isAsync) {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", KAFKA_SERVER_URL + ":" + KAFKA_SERVER_PORT);
        properties.put("bootstrap.servers", KAFKA_SERVER_URL + ":" + KAFKA_SERVER_PORT);
//        properties.put("client.id", CLIENT_ID);
        properties.put("acks", "all");
        properties.put("retries", 0);
        properties.put("batch.size", 30384);
        properties.put("linger.ms", 1);
        properties.put("buffer.memory", 33554432);
        properties.put("max.poll.records ", 5000);
        properties.put("queued.max.requests ", 10000);
        properties.put("queued.min.requests ", 5000);
        properties.put("fetch.max.wait.ms ", 0);
        properties.put("min.poll.records ", 5000);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        producer = new KafkaProducer(properties);
        this.topic = topic;
        this.isAsync = isAsync;
    }

    public static SampleProducer Get(){
        if(_instance == null){
            _instance = new SampleProducer("position_test", false);
        }
        return _instance;
    }

    public void send(String message) {
        producer.send(new ProducerRecord(this.topic, "position", message));
    }
}

