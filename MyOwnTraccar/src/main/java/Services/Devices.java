package Services;

import io.netty.buffer.ByteBuf;

import java.util.ArrayList;
import java.util.List;

public class Devices {
    private static Devices _instance = null;

    public List<Object> messages;
    public List<String> onlineDevices;

    private Devices(){
        onlineDevices = new ArrayList<>();
        messages = new ArrayList<>();
    }

    public static Devices Get(){
        if (_instance == null) {
            _instance = new Devices();
        }
        return _instance;
    }
}
