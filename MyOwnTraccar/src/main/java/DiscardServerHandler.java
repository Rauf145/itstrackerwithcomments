import Models.Message;
import Services.IService;
import Services.NatsRepository;
import Services.Service;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

import java.net.InetSocketAddress;

public class DiscardServerHandler extends ChannelInboundHandlerAdapter {
    private IService service = Service.Get();

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf in = (ByteBuf) msg; // data from client
        Message m = new Message(); //create message(this my class which contains :
        //Content - byte[] (device data)
        //Response - byte[] (response from DecoderWorker)
        //Type - String (not used now)
        //Imei - String (This value set after decoding)
        //Protocol - String (Port. For example : 5039 - Wialon)
        //Client - String (Device address)
        //LoggedIn - Boolean

        //get port
        String port = String.valueOf(((InetSocketAddress) ctx.channel().localAddress()).getPort());
        try {
            //if port  5039(Wialon)
            if (port.contains("5039")) {
                String message = "";
                //read message
                while (in.isReadable()) {
                    message += (char) in.readByte();
                }

                //split into small messages and send to Message Queue. This peace of code should be remove.Message should split in decoderWorker side
                String[] messages = message.split("\n");
                for (int i = 0; i < messages.length; i++) {
                    if (messages[i] != null && !messages[i].isEmpty()) {
                        m.Content = messages[i].getBytes();
                        m.Client = ctx.channel().remoteAddress().toString();
                        m.Protocol = port;
                        this.service.send(m);
                    }
                }
            } else {

                m.Content = new byte[in.readableBytes()];
                in.readBytes(m.Content);
                System.out.println(new String(m.Content, "UTF-8"));
                m.Protocol = port;
                m.Client = ctx.channel().remoteAddress().toString();
                this.service.send(m);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ReferenceCountUtil.release(msg); // (2)
        }
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //add connected device into list
        NatsRepository.GetInstance().AddChannel(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        //remove disconnected device from list
        NatsRepository.GetInstance().RemoveChannel(ctx.channel());
    }
}
