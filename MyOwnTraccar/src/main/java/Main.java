import Services.Devices;
import Services.IService;
import Services.Service;
import redis.clients.jedis.Jedis;

public final class Main {

    public static void main(String args[]){
        try {
            //run discard server(netty)
            new DiscardServer().run();

        }catch (Exception ex){
            System.out.println("Bind error(port or address)");
        }
    }
}
