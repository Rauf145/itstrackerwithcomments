package Interfaces;

import Models.Message;

public interface IProtocol {
    Message Decode(Message msg) throws Exception ;
}
