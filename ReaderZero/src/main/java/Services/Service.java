package Services;

import Interfaces.IProtocol;
import Models.Message;
import Protocols.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

import javax.swing.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Service implements IService {

    private static Service _instance = null;

    private Jedis jedis = null;
    private Pipeline p = null;

    private Map<String, IProtocol> Decoders = null;

    private Timer timer = null;

    private Gson objectMapper = null;


    private Service() {
        this.jedis = new Jedis("127.0.0.1", 6379);
        this.objectMapper = new Gson();
        this.p = this.jedis.pipelined();
        this.timer = new Timer(1000, (obj) -> {
            this.p.sync();
//            this.p.close();
//            this.jedis.close();
//            this.jedis = new Jedis();
//            p = this.jedis.pipelined();
        });

        this.Decoders = new HashMap<String, IProtocol>();
        this.Decoders.put("5039", new WialonProtocolDecoder());
//        this.Decoders.put("5037", new WialonProtocolDecoder());
        this.Decoders.put("5046", new RuptelaProtocolDecoder());
        this.Decoders.put("5080", new BceProtocolDecoder());
        this.Decoders.put("5055", new TeltonikaProtocolDecoder());
        this.timer.start();
    }

    public static Service Get() {
        if (_instance == null) {
            _instance = new Service();
        }
        return _instance;
    }

    @Override
    public Message Decode(Message msg) {
        Message result = null;
        try {
            //get decoder from dictionary (key - port, value - IProtocol)
            result = this.Decoders.get(msg.Protocol).Decode(msg);
            //check result
            if (result != null) {
                switch (result.Type) {
                    //if a single position
                    case "position":
                        String temp = new String(msg.Content, StandardCharsets.UTF_8);
                        this.p.lpush("ps", temp);
                        result.Content = null;
                        break;
                    //if a multiple positions
                    //you should improve this code
                    case "positions":
//                        List<Position> positions = this.objectMapper
//                                .fromJson(new String(msg.Response), new TypeToken<List<Position>>() {
//                                }.getType());
////                        for (int i = 0; i < positions.size(); i++) {
////
////                            this.p.lpush("ps", objectMapper.toJson(positions.get(i)));
////                        }
                        this.p.lpush(new String(msg.Response));
                        result.Response = null;
                        break;
                }
            }
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public void Send(Message msg) {

    }
}
