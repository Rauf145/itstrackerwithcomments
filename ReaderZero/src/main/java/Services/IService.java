package Services;

import Models.Message;

public interface IService {
    Message Decode(Message msg);
    void Send(Message msg);
}
