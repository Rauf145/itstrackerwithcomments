package Repository;

import Protocols.*;
import Services.IService;
import Services.Service;
import io.nats.client.*;
import io.nats.client.Message;
import org.codehaus.jackson.map.ObjectMapper;

import java.nio.charset.StandardCharsets;

public class NatsConsumer {


    //nats connection
    private Connection nc = null;

    //dispatcher for receiving from message queue
    private Dispatcher d = null;

    //create service
    private IService service = null;

    private ObjectMapper gson = null;

    //static instance of this class
    private static NatsConsumer _instance = null;

    private NatsConsumer() {
        this.service = Service.Get();
        this.gson = new ObjectMapper();

        try {
//            Options options = new Options.Builder().server("localhost:4222").
//                    maxControlLine(2 * 2024). // Set the max control line to 2k
//                    bufferSize(1000000).build();
            this.nc = Nats.connect("localhost:4222");
            this.d = nc.createDispatcher((msg) -> {
                this.Handle(msg);
            });
            this.d.subscribe("messages", "workers");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static NatsConsumer GetInstance(){
        if (_instance == null) {
            _instance = new NatsConsumer();
        }
        return _instance;
    }

    //ehtiyac yoxdur
    public String recieve() throws Exception {
        return "";
    }

    @Override
    public void finalize() throws Exception {
        this.nc.close();
    }

    private void Handle(Message msg) {
        try {
            //get data from message queue
            byte[] data = msg.getData();
            //convert from json Message object(my class)
            String json = new String(data, StandardCharsets.UTF_8);
            Models.Message message = gson.readValue(json, Models.Message.class);
            //send this message object to service and receive data from service
            Models.Message result = this.service.Decode(message);
//            if (result != null && result.Response != null) {
//                result.LoggedIn = true; //testing
//                json = gson.writeValueAsString(result);
//                byte[] arr = json.getBytes();
//            }
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    public void Send(Models.Message msg){
        try {
            byte[] data = this.gson.writeValueAsString(msg).getBytes();
            msg.Content = null;
            this.nc.publish("Response", data);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}