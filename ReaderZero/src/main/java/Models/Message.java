package Models;

public class Message
{
    public byte[] Content = null;
    public byte[] Response = null;
    public String  Type;
    public String  Imei;
    public String  Protocol;
    public String Client;
    public Boolean LoggedIn = false;
}