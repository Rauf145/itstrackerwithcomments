//package Models;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Properties;
//
//import Protocols.WialonProtocolDecoder;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.clients.consumer.ConsumerRecords;
//import org.apache.kafka.clients.consumer.KafkaConsumer;
//
//public class NotificationConsumerThread implements Runnable {
//
//    private final KafkaConsumer<String, String> consumer;
//    private final String topic;
//    private int index ;
//    private static int Count = 0;
//
//    Connection Connection = null;
//
//    Statement statement = null;
//
//    List<String> positions = null;
//
//    WialonProtocolDecoder decoder = null;
//
//
//    public NotificationConsumerThread(String brokers, String groupId, String topic, Integer index) {
//        String url = "jdbc:postgresql://127.0.0.1:5432/traccar";
//        String user = "postgres";
//        String password = "p@$$w0rd";
//
//        try {
//            Class.forName("org.postgresql.Driver");
//            this.Connection = DriverManager.getConnection(url, user, password);
//            this.statement = this.Connection.createStatement();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        this.positions = new ArrayList<>();
//        this.decoder = new WialonProtocolDecoder();
//
//        Properties prop = createConsumerConfig(brokers, groupId + index);
//        this.consumer = new KafkaConsumer<>(prop);
//        this.topic = topic;
//        this.index = index;
//        this.consumer.subscribe(Arrays.asList(this.topic));
//    }
//
//    private static Properties createConsumerConfig(String brokers, String groupId) {
//        Properties props = new Properties();
//        props.put("bootstrap.servers", brokers);
//        props.put("group.id", groupId);
//        props.put("enable.auto.commit", "true");
//        props.put("auto.commit.interval.ms", "1000");
//        props.put("session.timeout.ms", "30000");
//        props.put("auto.offset.reset", "earliest");
//        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
//        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
//        props.put("max.poll.records ", 5000);
//        props.put("queued.max.requests ", 10000);
//        props.put("queued.min.requests ", 5000);
//        props.put("fetch.max.wait.ms ", 0);
//        props.put("min.poll.records ", 5000);
//        return props;
//    }
//
//    @Override
//    public void run() {
//        while (true) {
//            ConsumerRecords<String, String> records = consumer.poll(100);
//            System.out.println(this.index + " - "  + records.count());
//            for (ConsumerRecord<String, String> record : records) {
//                try {
//                    String[] strs =  record.value().split("\r");
//                    for (int j = 0; j < strs.length; j++) {
//                        String str = decoder.decode(strs[j]).toString();
//                        this.positions.add(str);
//                        statement.addBatch(String.format("INSERT INTO tc_positions(" +
//                                "protocol, deviceid, servertime, devicetime, fixtime, valid, latitude, longitude, altitude) " +
//                                "VALUES (%s);", str.toString()));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
////            NotificationConsumerThread.Count += records.count();
//            if(this.positions.size() > 60000) {
//                this.positions.clear();
//                try {
//                    this.statement.executeBatch();
//                }catch (Exception e){e.printStackTrace();}
//
//
////                System.out.println(20000);
////                NotificationConsumerThread.Count = 0;
//            }
//        }
//
//    }
//
//
//}