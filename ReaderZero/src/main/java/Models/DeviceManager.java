package Models;

import java.util.Dictionary;
import java.util.Hashtable;

public class DeviceManager {
    private static DeviceManager _instance = null;

    private Dictionary<String, String> users = null;

    private DeviceManager() {
        this.users = new Hashtable<>();
    }

    public static DeviceManager GetManager() {
        if (_instance == null) {
            _instance = new DeviceManager();
            _instance.users.put("test", "12345");
        }
        return _instance;
    }


    public void Add(String clientAddress, String imei) {
        this.users.put(clientAddress, imei);
    }

    public String Get(String clientAddress) {
        return this.users.get(clientAddress);
    }
}
